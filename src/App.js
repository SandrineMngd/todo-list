import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {

// constantes

const [tasks, setTasks] = useState([])


// fonction pour afficher les tâches terminées
function displayFinishedTasks(){

}

// fonction onclick pour ajouter une tâche
function addTasks(task){
setTasks([... task])
}

// fonction pour gérer le onclick = bouton ajouter

function handleButtonClick(event) {
  const plusTask = event.target.value
  return console.log (addTasks(plusTask))
}

// fonction pour afficher au fur et à msure les tâches ajoutées
function displayTasksList(taskList) {
  return taskList.map(task =><div>{task}</div>)
}


  return (
    <div className="App">
    <div class="container">

      <div>
        <h2>Afficher les tâches terminées</h2>
        <button type="button" class="btn btn-info" onClick={handleButtonClick}>clic</button>
      </div>

    

      <div class="input-group-prepend" id="button">
      <div></div>
        <button class="btn btn-outline-secondary" type="button" id="add" onClick={handleButtonClick}>Ajouter</button>
        <input type="text" class="form-control" placeholder="" aria-label="Example text with two button addons" aria-describedby="button-addon3"></input>
      </div>

    {/* div pour afficher la liste des tâches */}
    <div></div>

    </div>
    </div>
  );
}




export default App;
